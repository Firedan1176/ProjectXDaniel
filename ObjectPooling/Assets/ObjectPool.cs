﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : Singleton<ObjectPool> {
	public int groupCount;
	public int objectCount;


	/// <summary>
	/// The Pool. This is a List of PoolGroups, which store a List of pooled objects.
	/// </summary>
	private Dictionary<string, PoolGroup> pool = new Dictionary<string, PoolGroup>();
	/// <summary>
	/// Contains all the child transforms in the hierarchy which are references for pooled objects.
	/// </summary>
	private Dictionary<PoolGroup, Transform> poolRepository = new Dictionary<PoolGroup, Transform>();


	/// <summary>
	/// The number of seconds between each scan.
	/// A scan is the process of determining if the pool can be reduced by observing memory, processing power, and/or framerate.
	/// </summary>
	private float scanRate = 5;

	/// <summary>
	/// Searches for a pool matching an ID that is passed, or creates a new pool if one doesn't exist.
	/// This is the most general form for creating a new pool.
	/// </summary>
	/// <param name="original">The reference GameObject that should be instantiated/enabled from the pool.</param>
	/// <param name="id">A unique ID (name or category) to pool the GameObject reference, as well as set the name of the Transform which holds the pooled objects. Each ID should be different, and no two should match.</param>
	/// <param name="safeCheck">Check to see if the pool already contains a PoolGroup with the original that is passed. This should generally be used, but can be a little performance-intensive.</param>
	/// <returns>The newly created or pooled GameObject that has been enabled.</returns>
	public GameObject Register(GameObject original, string id, int count = 1, bool safeCheck = true) {
		if(original == null) {
			Debug.LogWarning("[ObjectPool] Error: Cannot find or create a PoolGroup for null GameObjects.");
			return null;
		}
		if (string.IsNullOrEmpty(id)) {
			Debug.LogWarning("[ObjectPool] Error: An ID must be passed to find or create a PoolGroup.");
			return null;
		}

		PoolGroup group = null;
		GameObject obj = null;

		if (!pool.ContainsKey(id)) {
			//Check to see if there's already a PoolGroup that contains the original
			if(safeCheck)
				foreach (PoolGroup g in pool.Values) {
					if(g.group.Contains(original)) Debug.LogWarning("[ObjectPool] Warning: The pool already contains a PoolGroup for object '" + original.name + "'.");
				}

			if (count > 2048) {
				Debug.LogWarning("[ObjectPool] Error: Cannot create a pool greater than 2048 count.");
				return null;
			}

			
			Debug.Log("[ObjectPool] Debug: Creating a new pool for '" + original.name + "'");
			//Create a new pool
			group = new PoolGroup();
			group.id = id;
			//Create the Repository for it
			CreateRepository(group);
			//Ahead of time, create the first element of the new Group (to make sure 'group.group[0]' isn't null/nonexistent)
			CreateObject(group, original);
			//Start the creation process of the count (count - 1 because we made 1 ahead of time)
			StartCoroutine(PoolLoop(group, original, count - 1, 0.1f));
			//Set obj (what is returned) to original (which was just added)
			obj = group.group[0];
			//Add the PoolGroup, along with the id to the Pool Dictionary
			pool.Add(id, group);
			//Set its parent to null
			obj.transform.SetParent(null);
			//Enable it
			obj.SetActive(true);

			UpdateStats();

			//Return the go
			return obj;
		}
		else {
			//Set the temporary PoolGroup to the one that we match using a key (id).
			group = pool[id];
			if(group.group.Count == 0) {
				//The PoolGroup we found (which is good) is empty. TODO: Notify the user, but either create new pooled go's or delete this PoolGroup (memory/cpu/fps should determine).
				Debug.LogWarning("[ObjectPool] Error: Could not instantiate '" + original.name + "' because the previously created pool is empty.");
				return null;
			}
			Debug.Log("[ObjectPool] Debug: Found pool for '" + original.name + "'. Enabling and returning first instance.");

			//Set the go we return to the first element in the matched pool group
			obj = group.group[0];
			//Remove the first element in the matched pool group
			group.group.RemoveAt(0);
			//Set the element to active
			obj.SetActive(true);
			//Set its parent to null
			obj.transform.SetParent(null);
			//Enable it
			obj.SetActive(true);

			UpdateStats();

			//Return the go
			return obj;
		}

	}

	/// <summary>
	/// Return a GameObject back into a PoolGroup.
	/// Equivalent of GameObject.Destroy().
	/// Note: This implies that a pool was already created for this id.
	/// </summary>
	/// <param name="obj">The GameObject to be returned into a pool.</param>
	/// <param name="id">The unique ID of the PoolGroup.</param>
	/// <returns>True if the GameObject was pooled; False if a pool does not exist for the object.</returns>
	public bool Return(GameObject obj, string id) {
		if(!pool.ContainsKey(id)) return false;
		
		pool[id].group.Add(obj);
		obj.transform.SetParent(poolRepository[pool[id]]);
		obj.SetActive(false);
		return true;
	}

	/// <summary>
	/// Manually purge the pool to remove old, expired GameObjects.
	/// </summary>
	public void Purge() {
		UpdateStats();

		throw new System.NotImplementedException("This method has not been implemented yet.");
	}

	public void DestroyPoolGroup(PoolGroup group) {
		if(pool.ContainsKey(group.id)) pool.Remove(group.id);
		Destroy(poolRepository[group].gameObject);
		poolRepository.Remove(group);

		UpdateStats();
	}

	/// <summary>
	/// Manually purge the pool to remove old, expired GameObjects, limiting the percent size of the pool.
	/// </summary>
	/// <param name="percent">The percent of the pool to purge./param>
	public void Purge(float percent) {
		percent = Mathf.Clamp(percent, 0.01f, 1f);
		Debug.Log("[ObjectPool] Notice: Purging Pool to " + (percent * 100) + "%.");
		foreach(PoolGroup p in pool.Values) {
			Debug.Log("Purging " + (int)(p.group.Count * percent) + " elements.");
			for(int i = 0; i < (int)(p.group.Count * percent); i++) {
				Destroy(p.group[i]);
				p.group.RemoveAt(i);

			}
		}

		UpdateStats();
	}

	/// <summary>
	/// Completely clear the Pool.
	/// </summary>
	public void Clear() {
		pool.Clear();
		foreach(Transform t in poolRepository.Values) Destroy(t.gameObject);
		poolRepository.Clear();
		Debug.Log("[ObjectPool] Notice: Cleared pool.");
	}

	IEnumerator PoolLoop(PoolGroup reference, GameObject original, int count, float delay) {
		for(int i = 0; i < count & i < 4096; i++) {
			CreateObject(reference, original);
			UpdateStats();
			yield return new WaitForSeconds(delay);
		}
	}

	void UpdateStats() {
		groupCount = pool.Values.Count;

		int objCount = 0;
		foreach(PoolGroup p in pool.Values) objCount += p.group.Count;
		objectCount = objCount;
	}

	/// <summary>
	/// The most basic form of creating a new pooled object. This should only be used by the ObjectPool and not by any user in code.
	/// </summary>
	/// <param name="reference"></param>
	/// <param name="original"></param>
	private void CreateObject(PoolGroup reference, GameObject original) {
		//Allocate memory for the clone
		GameObject temp;
		//Simultaneously instantiate the clone, as well as add it to the PoolGroup
		reference.group.Add(temp = GameObject.Instantiate(original));
		//Disable the instantiated clone
		temp.SetActive(false);
		//Set the parent of the clone to the transform of its PoolGroup
		temp.transform.SetParent(poolRepository[reference]);

		UpdateStats();
	}


	/// <summary>
	/// Creates a new repository. This should only be used by the ObjectPool and not by any user in code.
	/// </summary>
	/// <param name="group">The reference group.</param>
	/// <param name="id">The name for the repository.</param>
	private void CreateRepository(PoolGroup group) {
		GameObject repo = new GameObject();
		poolRepository.Add(group, repo.transform);
		repo.name = group.id;
		repo.transform.SetParent(transform);
		repo.transform.localPosition = Vector3.zero;

		UpdateStats();
	}
}

/// <summary>
/// A List of GameObjects which store pooled objects.
/// </summary>
public class PoolGroup {
	public string id;
	public List<GameObject> group = new List<GameObject>();
	/// <summary>
	/// The time in seconds since the PoolGroup was created. Used by the system for cleanup/garbage collection.
	/// </summary>
	public int life;

	/// <summary>
	/// Prevents the PoolGroup from being destroyed if empty.
	/// Note: The PoolGroup must be manually destroyed.
	/// </summary>
	public bool protect;
}