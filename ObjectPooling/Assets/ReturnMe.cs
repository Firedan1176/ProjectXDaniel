﻿using UnityEngine;
using System.Collections;

public class ReturnMe : MonoBehaviour {

	public bool returnMe;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(returnMe) {
			returnMe = false;
			ObjectPool.Instance.Return(gameObject, "Group1");
		}
	}
}
