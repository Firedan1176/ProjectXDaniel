﻿using UnityEngine;
using System.Collections;

public class PoolTest : MonoBehaviour {

	public bool testPool = false;
	public bool purge = false;
	public bool clear = false;
	public float percentPurge = 0.5f;
	public GameObject objToPool;
	public string id = "New Group 1";
	[Range(1, 100)]
	public int count;

	void Update () {
		if(testPool) {
			testPool = false;
			ObjectPool.Instance.Register(objToPool, id, count);
		}
		if(purge) {
			purge = false;
			ObjectPool.Instance.Purge(percentPurge);
		}
		if(clear) {
			clear = false;
			ObjectPool.Instance.Clear();
		}
	}
}
