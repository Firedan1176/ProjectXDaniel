﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;


public enum AIAction {None, Attacking, Defending};
public enum ThreatType {Default, Main, AI};

public class AIController : MonoBehaviour {

	public GameObject head;
	public GameObject weapon;
	NavMeshAgent myAgent;

	public List<AIController> visibleAIs = new List<AIController>();
	public int targetHierarchyElement = 0; //The current index of the target AIController from above

	public List<TargetObject> targetHierarchy = new List<TargetObject>();

	Coverpoint targetCoverpoint;
	public Vector3 lastSeenEnemyPosition;

	//NOTE: Replaced with a helper method.
	//public List<int> threatValues = new List<int>();

	public int health;

	public bool shouldTakeCover = false;
	public float takeCoverRate = 0.5f;
	public float takeCoverCountdown;
	public float coverRange;

	public bool enableTargeting = true;
	public int range;
	public AIAction currentAction;

	public float refreshRate = 0.2f;
	private float refreshRateCountdown;

	MeshRenderer myRenderer;
	public Mesh arrowMesh;
	Animator gunAnimator;
	public Team team;

	public float fireRate;
	public float fireRateCountdown;

	RaycastHit hitInfo;

	public bool isFiring = false;
	public int bulletDamage = 10;

	Vector3 offsetDist = new Vector3(0, 0.8f, 0);

	// Use this for initialization
	void Start () {
		myAgent = GetComponent<NavMeshAgent>();
		myRenderer = GetComponentInChildren<MeshRenderer>();
		refreshRateCountdown = refreshRate;
		fireRateCountdown = fireRate;
		gunAnimator = head.GetComponent<Animator>();

		myRenderer.material.color = team.teamColor;
		//targetHierarchy.Add(new TargetObject(GameManager.Instance.GetEnemyTeam(team).gameObject, GetThreatLevel(GameManager.Instance.GetEnemyTeam(team))));
		targetHierarchy.Add(GetTargetObject(GameManager.Instance.GetEnemyTeam(team)));
	}
	
	// Update is called once per frame
	void Update () {

		if(visibleAIs.Count > 0 && visibleAIs[targetHierarchyElement] != null)
			if(CanSee(visibleAIs[targetHierarchyElement])) {
				isFiring = true;
		}
		else isFiring = false;

		if (refreshRateCountdown <= 0) {
			TargetLoop();
			refreshRateCountdown = refreshRate;
		}

		shouldTakeCover = (isFiring && HasNearbyCover() ? true : false);

		//When shouldTakeCover is true, we will scan nearby cover points every *countdown* seconds.
		if (shouldTakeCover) {
			if(takeCoverCountdown <= 0) {
				takeCoverCountdown = takeCoverRate;
				TakeCover();
			}
			takeCoverCountdown -= Time.deltaTime;
		}
		else if(visibleAIs.Count == 0) myAgent.destination = lastSeenEnemyPosition; //If we shouldn't be taking cover and no nearby AIs, go to the last AI pos seen.

		refreshRateCountdown -= Time.deltaTime;
		if(fireRateCountdown <= 0 && isFiring && HasTarget()) {
			Fire();
			fireRateCountdown = fireRate;
		}
		if(currentAction != AIAction.None) fireRateCountdown -= Time.deltaTime;
	}

	void RotateFaceTarget() {
		Quaternion rot = Quaternion.LookRotation(visibleAIs[targetHierarchyElement].transform.position - transform.position);
		rot.eulerAngles = new Vector3(rot.eulerAngles.x, 0, 0);
		weapon.transform.localRotation = rot;
		rot = Quaternion.LookRotation(visibleAIs[targetHierarchyElement].transform.position - transform.position);
		rot.eulerAngles = new Vector3(0, rot.eulerAngles.y, 0);
		head.transform.localRotation = Quaternion.Euler(rot.eulerAngles + transform.eulerAngles);
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, range);
		
		if(shouldTakeCover) {
			Gizmos.color = Color.cyan;
			//TODO: Do something about this. It will draw a fancy colored gizmo over the target coverpoint, but it's kind of lazy.
			Gizmos.DrawCube(new Vector3(myAgent.destination.x, 0, myAgent.destination.z) + Vector3.up * 2, new Vector3(1, 0, 1));
		}

		//Draw debugging stuff for the currently selected AI.
		if (visibleAIs.Count > 0) {


			foreach(AIController ai in visibleAIs) {
				if(ai == null) continue; //We are trying to draw an enemy that was killed the last UPDATE frame. Since update it called less than OnDrawGizmos(), it will try to use the AI before "Cleanup" (remove the null one in DetectEnemies()).
				Gizmos.color = (ai == visibleAIs[targetHierarchyElement] ? Color.red : Color.yellow);	//Hightlight the target AI with a Red line, or a Yellow line for all others.
				//if(ai.team == team) Gizmos.color = Color.gray; //visibleAIs is only enemy AIs. If you make visibleAIs *all* AIs (even teammates), uncomment this.
				Gizmos.DrawLine(transform.position, ai.transform.position);
				Vector3 lookVector = ai.transform.position - transform.position;
				if(lookVector != Vector3.zero)
					Gizmos.DrawMesh(arrowMesh, transform.position + (ai.transform.position - transform.position).normalized, Quaternion.LookRotation(lookVector));

				DrawHandlesLine(GetTargetObject(ai).threatLevel.ToString(), Vector3.Lerp(ai.transform.position, transform.position, 0.5f));

			}
		}
	}

	void OnDrawGizmos() {
		//Gizmos.color = team.teamColor;
		//Gizmos.DrawLine(transform.position, transform.position + (Vector3.up * 10));
		//Gizmos.DrawSphere(transform.position + (Vector3.up * 10), 0.5f);
		Gizmos.DrawIcon(transform.position + (Vector3.up * 10), "PlayerIcon");
	}

	void Fire() {
		GetComponent<AudioSource>().Play();
		gunAnimator.SetTrigger("Fire");
		RotateFaceTarget();
		Debug.DrawLine(weapon.transform.position, weapon.transform.position + weapon.transform.forward * 20, Color.white, 0.2f);
		if(Physics.Raycast(weapon.transform.position, weapon.transform.forward, out hitInfo, 100)) {
			//Debug.Log("HIT! " + hitInfo.transform.name);

			//TODO: Refactor. Make sure the AIController has a parent, without this first one, you'll get some null exceptions.
			if(hitInfo.transform.parent)
				if(hitInfo.transform.parent.GetComponent<AIController>())
					hitInfo.transform.parent.GetComponent<AIController>().TakeDamage(bulletDamage);
		}
	}

	/// <summary>
	/// Will set the NavAgent's destination to the closest and safest coverpoint.
	/// </summary>
	void TakeCover() {

		Coverpoint targetPoint = null;
		float coverBenefit = 0; //How beneficial the targetPoint is

		foreach(Coverpoint p in FindObjectsOfType<Coverpoint>()) {
			if(DistanceTo(p.gameObject) <= coverRange) {
				if (targetPoint == null) targetPoint = p;

				float benefit = GetCoverBenefit(p);
				if(benefit > coverBenefit) {
					targetPoint = p;
					coverBenefit = benefit;
				}
			}
		}

		if(targetPoint) targetCoverpoint = targetPoint; //Assign a target coverpoint, instead of just setting the NavAgent's destination.

		myAgent.destination = targetCoverpoint.transform.position;
	}


	/// <summary>
	/// Will check all enemies shooting at the player and determine the best cover from them.
	/// </summary>
	/// <param name="point"></param>
	/// <returns></returns>
	float GetCoverBenefit(Coverpoint point) {
		if(!HasTarget()) return 0;
		float benefit = range - DistanceTo(point.gameObject);
		RaycastHit tempHitInfo;
		if(Physics.Raycast(point.transform.position + Coverpoint.coverRaycastPosition, (visibleAIs[targetHierarchyElement].transform.position - point.transform.position) + Coverpoint.coverRaycastPosition, out tempHitInfo)) {
			Debug.DrawRay(point.transform.position + Coverpoint.coverRaycastPosition, (visibleAIs[targetHierarchyElement].transform.position - point.transform.position) + Coverpoint.coverRaycastPosition);
			if(!tempHitInfo.transform.GetComponentInParent<AIController>()) {
				benefit += benefit * 3;
				//Debug.Log("Blocking " + benefit + ": " + point.gameObject.name + ", which hit " + tempHitInfo.transform.name);
			}
		}
		return benefit;
	}

	TargetObject GetTargetObject(Team team) {
		float threat = (range - Vector3.Distance(transform.position, team.transform.position));
		return new TargetObject(team.gameObject, threat, ThreatType.Main);
	}

	TargetObject GetTargetObject(AIController ai) {
		float threat = (range - Vector3.Distance(transform.position, team.transform.position)) * (ai.currentAction == AIAction.Attacking?GameManager.Instance.threatLevel_Attacking:GameManager.Instance.threatLevel_None);
		return new TargetObject(ai.gameObject, threat, ThreatType.AI);
	}


	bool HasNearbyCover() {
		List<Coverpoint> temp = FindObjectsOfType<Coverpoint>().ToList();
		List<Coverpoint> nearbys = new List<Coverpoint>();
		foreach(Coverpoint n in temp) if(DistanceTo(n.gameObject) <= coverRange) nearbys.Add(n); 
		return (nearbys.Count == 0)?false:true;
	}

	public void TakeDamage(int damage) {
		health -= damage;
		if(health <= 0)
			KillPlayer();
	}

	public void KillPlayer() {
		GameObject.Destroy(gameObject);
	}

	void TargetLoop() {
		if(currentAction == AIAction.None) return;
		DetectTargets();
		UpdateHierarchy();

		//myAgent.Stop(); //This will make the AI stop if there's no visible AI's.
		//return; //Instead of returning, we should go to the highest threat value non-ai.
		if(targetHierarchy.Count > 0) if(targetHierarchy[0].threatType == ThreatType.Main) myAgent.SetDestination(targetHierarchy[0].targetGameObject.transform.position);

		targetHierarchyElement = 0; //Refresh the target hierarchy index.

		for(int i = 0; i < targetHierarchy.Count; i++) {
			//This used to be visibleAIs and targetAi, but updated with targetHierarchy version.
			if (targetHierarchy[i].threatLevel > targetHierarchy[targetHierarchyElement].threatLevel) {
				//Debug.Log("Reassigned target.");
				targetHierarchyElement = i;
			}
		}

		if (targetHierarchy.Count > 0) if (targetHierarchy[targetHierarchyElement] == null) Debug.LogError("Assigned new target, but it's null for some reason: " + targetHierarchy[targetHierarchyElement] + " at index: " + targetHierarchyElement + " with size of: " + targetHierarchy.Count);

		if (targetHierarchy.Count > 0) if(targetHierarchy[targetHierarchyElement].threatType == ThreatType.AI) if(CanSee(targetHierarchy[targetHierarchyElement].targetGameObject.GetComponent<AIController>())) lastSeenEnemyPosition = targetHierarchy[targetHierarchyElement].targetGameObject.transform.position;
		if(!shouldTakeCover) myAgent.SetDestination(targetHierarchy[0].targetGameObject.transform.position);
	}

	//NOTE: This will run through all AI's. If you have a lot, it'd be better to use
	//Physics.SphereCast, since you'll only get the surrounding AI's colliders. But then again that's performance costly...
	//Renamed from FindNewAIs(), since it "should" do more than just finding AI's (in the future).
	void DetectTargets() {

		visibleAIs = FindObjectsOfType<AIController>().ToList();
		for(int i = 0; i < visibleAIs.Count; i++) {
			if(visibleAIs[i] == this || visibleAIs[i].team == team || !CanSee(visibleAIs[i]) || DistanceTo(visibleAIs[i].gameObject) > range) {
				//if(name.Equals("AI Attacker 1")) Debug.Log(this.name + " = " + visibleAIs[i].name + " | " + visibleAIs[i].team + " = " + team + " | " + "CanSee = " + CanSee(visibleAIs[i]) + " | " + "CloseEnough = " + DistanceTo(visibleAIs[i].gameObject));
				visibleAIs.RemoveAt(i);
				i--;
			}
			else targetHierarchy.Add(GetTargetObject(visibleAIs[i]));
		}
	}



	void DrawHandlesLine(string text, Vector3 position, Color ? color = null) {
		UnityEditor.Handles.BeginGUI();
		UnityEditor.SceneView sv = UnityEditor.SceneView.currentDrawingSceneView;
		//Vector3.Lerp(visibleAIs[i].transform.position + offsetDist, transform.position + offsetDist, 0.5f)
		Vector3 screenPos = sv.camera.WorldToScreenPoint(position);
		Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
		if(color.HasValue) GUI.color = color.Value;
		GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + sv.position.height, size.x, size.y), text);
		UnityEditor.Handles.EndGUI();

	}

	/// <summary>
	/// Updates the target hierarchy.
	/// </summary>
	void UpdateHierarchy() {
		List<TargetObject> oldHierarchy = targetHierarchy;
		for(int i = 0; i < oldHierarchy.Count; i++) {
			//Update threat values (updates the TargetObjects, really...)
			if(oldHierarchy[i].threatType == ThreatType.AI)			oldHierarchy[i] = GetTargetObject(oldHierarchy[i].targetGameObject.GetComponent<AIController>());
			else if(oldHierarchy[i].threatType == ThreatType.Main)	oldHierarchy[i] = GetTargetObject(oldHierarchy[i].targetGameObject.GetComponent<Team>());
		}
		targetHierarchy = oldHierarchy;
		SortHierarchy();
	}

	/// <summary>
	/// Sorts the values in the TargetHierarchy.
	/// </summary>
	void SortHierarchy() {

		//No reason to sort if we have only 1 element.
		if(targetHierarchy.Count < 2) return;
		List<TargetObject> newList = targetHierarchy;

		for(int x = 0; x < newList.Count; x++) {
			for(int y = 0; y < newList.Count; y++) {
				if(newList[x].threatLevel < newList[y].threatLevel) {
					TargetObject tempObject = newList[y];
					newList[y] = newList[x];
					newList[x] = tempObject;
				}
			}
		}
		targetHierarchy = newList;
	}

	bool CanSee(AIController ai) {
		Ray ray = new Ray(head.transform.position, (ai.head.transform.position - head.transform.position));
		RaycastHit tempHitInfo;
		if (Physics.Raycast(ray, out tempHitInfo)) {
			if (tempHitInfo.transform.GetComponentInParent<AIController>() == ai) {
				return true;
			}
		}
		return false;	
	}

	bool HasTarget() {
		if(targetHierarchyElement > visibleAIs.Count - 1) return false;
		if(visibleAIs[targetHierarchyElement] == null) return false;
		return true;
	}

	float DistanceTo(GameObject obj) {
		return Vector3.Distance(transform.position, obj.transform.position);
	}
}

/// <summary>
/// Stores references and information about a target AI, team, or other task.
/// </summary>
[System.Serializable]
public class TargetObject {
	public GameObject targetGameObject;
	public float threatLevel;
	public ThreatType threatType;

	public TargetObject(GameObject targetGameObject, float threatLevel, ThreatType threatType) {
		this.targetGameObject = targetGameObject;
		this.threatLevel = threatLevel;
		this.threatType = threatType;
	}
}