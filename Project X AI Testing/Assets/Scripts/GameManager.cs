﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	private static GameManager instance = null;

	public static Team[] teams = new Team[2];

	public int threatLevel_BaseAttacking = 5;
	public int threatLevel_Attacking = 4;
	public int threatLevel_Defending = 2;
	public int threatLevel_None = 1;

	public int threatLevel_Team = 1;


	public static GameManager Instance {
		get {
			if(instance == null) {
				instance = FindObjectOfType<GameManager>();
			}
			if(instance == null) {
				GameObject obj = new GameObject("GameManager");
				instance = obj.AddComponent<GameManager>();
				Debug.Log("Couldn't find a GameManager; Making one.");
			}
			Init ();
			return instance;
		}
	}

	void OnApplicationQuit() {
		instance = null;
	}

	public Team GetEnemyTeam(Team myTeam) {
		return(myTeam == teams[0]?teams[1]:teams[0]);
	}


	static void Init () {
		teams = FindObjectsOfType<Team>();
	}
}
