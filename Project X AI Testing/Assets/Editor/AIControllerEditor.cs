﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


[CustomEditor(typeof(AIController))]
public class AIControllerEditor : Editor {
	
	//SerializedObject serObj;

	//SerializedProperty myAgent;
	//SerializedProperty team;
	//SerializedProperty health;

	//SerializedProperty enableTargeting;
	//SerializedProperty targetHierarchy;
	//SerializedProperty visibleAis;
	//SerializedProperty targetAi;
	//SerializedProperty range;
	//SerializedProperty currentAction;

	//SerializedProperty refreshRate;
	//SerializedProperty fireRate;
	//SerializedProperty isFiring;

	//SerializedProperty bulletDamage;
	//SerializedProperty offsetDist;
	//SerializedProperty head;
	//SerializedProperty weapon;
	//SerializedProperty arrowMesh;

	//// Use this for initialization
	//void OnEnable () {
	//	serObj			= new SerializedObject(target);

	//	team			= serObj.FindProperty("team");
	//	health			= serObj.FindProperty("health");
	//	enableTargeting = serObj.FindProperty("enableTargeting");
	//	targetHierarchy = serObj.FindProperty("targetHierarchy");
	//	visibleAis		= serObj.FindProperty("visibleAIs");
	//	targetAi		= serObj.FindProperty("targetAi");
	//	range			= serObj.FindProperty("range");
	//	currentAction	= serObj.FindProperty("currentAction");
	//	refreshRate		= serObj.FindProperty("refreshRate");
	//	fireRate		= serObj.FindProperty("fireRate");
	//	isFiring		= serObj.FindProperty("isFiring");
	//	bulletDamage	= serObj.FindProperty("bulletDamage");
	//	head			= serObj.FindProperty("head");
	//	weapon			= serObj.FindProperty("weapon");
	//	arrowMesh		= serObj.FindProperty("arrowMesh");

	//}

	//// Update is called once per frame
	//public override void OnInspectorGUI() {
	//	serObj.Update();

	//	EditorGUILayout.PropertyField(team, new GUIContent("Team", "My team I am on."));
	//	EditorGUILayout.PropertyField(health, new GUIContent("Health", "My health."));
	//	EditorGUILayout.PropertyField(enableTargeting, new GUIContent("Targeting Enabled", "Choose whether AI Target Tracking is currently enabled."));
	//	EditorGUILayout.PropertyField(range, new GUIContent("Search Range", "The search radius of the AI to find viewable enemy AIControllers."));
	//	LabelProperty(currentAction.enumDisplayNames[currentAction.enumValueIndex], new GUIContent("Current Action", "The current action of the AIController."));
	//	EditorGUILayout.PropertyField(refreshRate, new GUIContent("Refresh Rate", "The rate at which the AI searches for new enemies."));
	//	EditorGUILayout.PropertyField(fireRate, new GUIContent("Fire Rate", "The rate at which the AIController fires."));
	//	LabelProperty(isFiring.boolValue?"True":"False", new GUIContent("Is Firing", "Is the AIController firing?"));
	//	EditorGUILayout.PropertyField(bulletDamage, new GUIContent("Bullet Damage", "The damage a bullet applies to an AIController."));
	//	EditorGUILayout.PropertyField(head, new GUIContent("Head", "The Head GameObject. This will rotate on the X axis."));
	//	EditorGUILayout.PropertyField(weapon, new GUIContent("Weapon", "The weapon that rotates on the X axis."));

	//	LabelProperty(targetAi.intValue.ToString(), new GUIContent("Target AI Index", "The index of the target AIController."));
	//	SerializedProperty sp = targetHierarchy.Copy();
	//	if(sp.isArray) {
	//		int length = 0;
	//		sp.Next(true);
	//		sp.Next(true);
	//		length = sp.intValue;
	//		sp.Next(true);
	//		List<string> values = new List<string>(length);
	//		int lastIndex = length - 1;
	//		for(int i = 0; i < length; i++) {
	//			//values.Add(sp.stringValue);

	//			//TODO: Clean this up!
	//			AIController temp = (AIController)serObj.targetObject;
	//			GameObject temp2 = (GameObject)sp.objectReferenceValue;
	//			values.Add(temp.CalculateThreatValue(temp2.GetComponent<AIController>()) + " : " + sp.objectReferenceValue.name);
	//			if(i < lastIndex) sp.Next(false);
	//		}
	//		EditorGUILayout.BeginHorizontal();
	//		EditorGUILayout.LabelField("Target Hierarchy", GUILayout.MaxWidth(110));
	//		EditorGUILayout.BeginVertical("Button");
	//		if(length != 0)
	//			for(int i = 0; i < values.Count; i++) {
	//				EditorGUILayout.LabelField("[ " + i + " ] " + values[i], EditorStyles.boldLabel);
	//			}
	//		else EditorGUILayout.LabelField("Hierarchy Empty.", EditorStyles.boldLabel);
	//		EditorGUILayout.EndVertical();
	//		EditorGUILayout.EndHorizontal();
	//	}

	//	serObj.ApplyModifiedProperties();
	//}

	//static void LabelProperty(string display, GUIContent information) {
	//	EditorGUILayout.BeginHorizontal();
	//	EditorGUILayout.LabelField(information);
	//	EditorGUILayout.LabelField(display);
	//	EditorGUILayout.EndHorizontal();
	//}
}
