﻿using UnityEngine;
using System.Collections;

public enum CoverStance {Standing, Crouching};
public class Coverpoint : MonoBehaviour {

	public CoverStance stance;
	public static Vector3 coverRaycastPosition = new Vector3(0, 0.3f, 0);
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube(transform.position + Vector3.up, new Vector3(1, 2, 1));
	}
}
