﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoverpointManager : MonoBehaviour {

	List<Coverpoint> myCoverpoints = new List<Coverpoint>();

	// Use this for initialization
	void OnDrawGizmosSelected() {
		ScanCoverpoints(false);
		
		foreach(Coverpoint c in myCoverpoints) {
			Gizmos.color = Color.green;
			Gizmos.DrawCube(c.transform.position + Vector3.up * 0.001f, new Vector3(1, 0, 1));
			Gizmos.color = Color.blue;
			Gizmos.DrawWireCube(c.transform.position + Vector3.up, new Vector3(1, 2, 1));
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(c.transform.position + Coverpoint.coverRaycastPosition, 0.1f);

		}
	}

	void Start() {
		ScanCoverpoints(false);

	}

	void ScanCoverpoints(bool force) {
		if(force) {
			myCoverpoints = GetComponentsInChildren<Coverpoint>().ToList();
			return;
		}

		if (myCoverpoints.Count == 0) {
			myCoverpoints = GetComponentsInChildren<Coverpoint>().ToList();
		}
	}
}
