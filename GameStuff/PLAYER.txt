PLAYER
=====================

Name: String
CurrentWeapon: Gun
Guns[]: Guns
Tactical: Tactical (grenades, throwable)
Equipment: Equipment (pick ups)

MaxFireRate: 10/s
Health: 100
Power: 1000
Speed: [0, 1, 2, 3, 4]

CameraHeight: float
State: enum (crouch, running, etc)


Alert: Collider


-----------------

Player.fire {
    if (guns[active].shoot(this.power)) {
        this.power -= gun.powerNeeded
    }
}
