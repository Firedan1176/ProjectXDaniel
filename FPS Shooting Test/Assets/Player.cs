﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public int health = 100;

	void Start() {
	}

	public void TakeHealth(int amount) {
		Debug.Log("Taking " + amount + " health!");
		health -= amount;
		if(health <= 0) Kill();
	}

	public void Kill() {
		Debug.Log("Killed!");
		health = 0;
		GetComponent<Renderer>().material.color = Color.red;
	}
}
