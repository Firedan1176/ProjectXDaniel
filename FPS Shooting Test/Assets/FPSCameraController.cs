﻿using UnityEngine;
using System.Collections;

public class FPSCameraController : MonoBehaviour {

	public Vector2 minBlowback;
	public Vector2 maxBlowback;
	public float blowbackMultiplier;
	public float recoverySpeed = 1;
	public bool onlyRecoverAfterShooting;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if((onlyRecoverAfterShooting && GetComponentInChildren<GunController>().isFiring == false) || !onlyRecoverAfterShooting)
		transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, Time.deltaTime * recoverySpeed);
	}

	public void AddBlowback(float amount) {
		amount = Mathf.Clamp(amount, 0f, 1f);
		transform.localRotation = Quaternion.Euler(transform.localEulerAngles + new Vector3(Random.Range(minBlowback.x, maxBlowback.x), Random.Range(minBlowback.y, maxBlowback.y), 0) * (amount * blowbackMultiplier));
	}
}
