﻿using UnityEngine;
using System.Collections;

/*
TODO
1.) Create enum to select the type of gun



*/

public class GunController : MonoBehaviour {

	public float fireRate;
	private float fireRate_Countdown;
	public float maxDistance;
	public bool isFiring;
	[Range(0, 1f)]
	public float blowbackAmount;
	public int damage;

	public float accuracy;

	Ray ray;
	public LayerMask layers;

	public Sprite bulletHoleSprite;
	public Vector3 bulletHoleSize;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
			isFiring = Input.GetAxisRaw("Fire1") == 1;
		if(isFiring && fireRate_Countdown <= 0) Fire();
		
		fireRate_Countdown -= Time.deltaTime;
	}

	void Fire() {

		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		ray.direction += new Vector3(Random.Range(-1, 1), Random.Range(-1, 1)) * accuracy;
		RaycastHit[] hitInfo = Physics.RaycastAll(ray, maxDistance, layers, QueryTriggerInteraction.Ignore); 
		
		GetComponent<AudioSource>().Play();

		foreach(RaycastHit hit in hitInfo) {
			Debug.Log("Hit " + hit.transform.name);
			GameObject bulletHole = new GameObject("BulletHole");
			SpriteRenderer sr = bulletHole.AddComponent<SpriteRenderer>();
			sr.sprite = bulletHoleSprite;
			sr.transform.localScale = bulletHoleSize;
			bulletHole.transform.position = hit.point + hit.normal * 0.01f;
			bulletHole.transform.forward = hit.normal;

			if(hit.transform.GetComponent<Player>()) {
				hit.transform.GetComponent<Player>().TakeHealth(damage);
			}
		}

		GetComponentInParent<FPSCameraController>().AddBlowback(blowbackAmount);

		fireRate_Countdown = fireRate;
	}
}
