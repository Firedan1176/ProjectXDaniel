﻿using UnityEngine;
using System.Collections;


/// <summary>
/// An object class that allows the use of a simple array of sounds, along with some handy functions.
/// </summary>
[System.Serializable]
public class SoundGroup {
	public AudioClip[] sounds;


	/// <summary>
	/// Returns a random element in the public array of sounds.	
	/// </summary>
	/// <returns></returns>
	public AudioClip GetRandom() {
		if(sounds.Length == 0) return null;
		return sounds[Random.Range(0, sounds.Length)];
	}
}


//This object class "SoundGroup" above should really be used for footstep sounds, gun sounds, etc.