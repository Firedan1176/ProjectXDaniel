﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum PowerState {None, On, Off};

public class DroneController : MonoBehaviour {

	Commander myCommander;
	public PowerState power = PowerState.None;

	public Camera myCamera;

	public GameObject targetIcon;
	public GameObject spaceLaunchObject;
	public bool isTargetting;

	[Range(-1, 1)]
	public float moveX, moveY, zoom;
	public bool invertX, invertY, invertZoom;
	public float lookSpeed = 0.2f, zoomSpeed = 0.2f;
	public Vector2 lookSpeedRange;
	public Vector2 zoomRange;
	public Vector4 lookRange;
	public Vector3 cameraLocalRotation;
	public GameObject center;
	public float moveSpeed, targetIconMoveSpeed;

	public Text zoomText;
	public Text posXText;
	public Text posYText;
	public Text teamPowerText;

	public int interfaceFramerate = 24;

	public bool drawGizmos;
	[Range(5, 128)]
	public int drawIterations;

	public AudioSource startupSound;
	public AudioSource loopSoundAmbience;
	public AudioSource loopSoundCameraMove;

	public LineRenderer laserTargetLR;


	// Use this for initialization
	void Start () {
		myCommander = GetComponent<Commander>();
		transform.localEulerAngles = Vector3.zero;
		StartCoroutine(UpdateInterfaceElements());
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround(center.transform.position, Vector3.up, Time.deltaTime * moveSpeed);

		myCamera.transform.Rotate(new Vector3(moveY, moveX, 0) * Mathf.Clamp(lookSpeed, lookSpeedRange.x, lookSpeedRange.y)); //y and x are backwards just because that's how rotations work
		myCamera.transform.localRotation = Quaternion.Euler(new Vector3(Mathf.Clamp(myCamera.transform.localEulerAngles.x, lookRange.x, lookRange.y), ClampAngle(myCamera.transform.localEulerAngles.y, lookRange.z, lookRange.w), 0));
		myCamera.fieldOfView = Mathf.Clamp(myCamera.fieldOfView + (zoom * zoomSpeed * Time.deltaTime), zoomRange.x, zoomRange.y);
		cameraLocalRotation = myCamera.transform.localEulerAngles;

		if(isTargetting)
			DisplayTargetIcon();

		targetIcon.SetActive(isTargetting);

		moveX = Mathf.RoundToInt(Input.GetAxisRaw("Horizontal")) * (invertX ? -1 : 1);
		moveY = Mathf.RoundToInt(Input.GetAxisRaw("Vertical")) * (invertY ? -1 : 1);
		zoom = Mathf.RoundToInt(Input.GetAxisRaw("Strafe")) * (invertZoom ? -1 : 1);

		if((moveX != 0 || moveY != 0 || zoom != 0) && !loopSoundCameraMove.isPlaying) loopSoundCameraMove.Play();
		else if ((moveX == 0 && moveY == 0 && zoom == 0) && loopSoundCameraMove.isPlaying) loopSoundCameraMove.Stop();


		if (power == PowerState.On) {
			GameObject.Find("Canvas/Commander").GetComponent<Animation>().Play("Commander_Startup");
			startupSound.Play();
			loopSoundAmbience.PlayDelayed(startupSound.clip.length);
			power = PowerState.None;
		}


		if(laserTargetLR.enabled) {
			RaycastHit hit;
			if(IfRaycastFromDrone(out hit))
				laserTargetLR.SetPosition(1, transform.InverseTransformPoint(hit.point));
		}
	}

	static float ClampAngle(float angle, float min, float max) {
		if (min < 0 && max > 0 && (angle > max || angle < min)) {
			angle -= 360;
			if (angle > max || angle < min) {
				if (Mathf.Abs(Mathf.DeltaAngle(angle, min)) < Mathf.Abs(Mathf.DeltaAngle(angle, max))) return min;
				else return max;
			}
		} else if (min > 0 && (angle > max || angle < min)) {
			angle += 360;
			if (angle > max || angle < min) {
				if (Mathf.Abs(Mathf.DeltaAngle(angle, min)) < Mathf.Abs(Mathf.DeltaAngle(angle, max))) return min;
				else return max;
			}
		}

		if (angle < min) return min;
		else if (angle > max) return max;
		else return angle;
	}

	void DisplayTargetIcon() {
		RaycastHit hit;
		if(IfRaycastFromDrone(out hit)) {
			targetIcon.transform.position = Vector3.Lerp(targetIcon.transform.position, hit.point + (hit.normal * 0.1f), targetIconMoveSpeed * Time.deltaTime);
			targetIcon.transform.up = Vector3.Lerp(targetIcon.transform.up, hit.normal, targetIconMoveSpeed * Time.deltaTime);
		}
	}


	public bool IfRaycastFromDrone(out RaycastHit hit) {
		Ray r = myCamera.ScreenPointToRay(Input.mousePosition);
		if(Physics.Raycast(r, out hit, 10000)) {
			return true;
		}
		Vector3 z = Vector3.zero;
		return false;
	}

	void OnDrawGizmos() {
		if(drawGizmos) {
			float radius = Vector3.Distance(
				new Vector3(transform.position.x, 0, transform.position.z),
				new Vector3(center.transform.position.x, 0, center.transform.position.z));
			Vector2 oldCoord = Vector2.zero;
			Vector2 newCoord;
	
			Gizmos.color = Color.white;
			for(int i = 0; i < drawIterations; i++) {
				if(i == 0) oldCoord = new Vector2(radius * Mathf.Cos(2 * Mathf.PI * i / drawIterations), radius * Mathf.Sin(2 * Mathf.PI * i / drawIterations));
				newCoord = new Vector2(radius * Mathf.Cos(2 * Mathf.PI * i / drawIterations), radius * Mathf.Sin(2 * Mathf.PI * i / drawIterations));
				Gizmos.DrawLine(new Vector3(oldCoord.x, transform.position.y, oldCoord.y), new Vector3(newCoord.x, transform.position.y, newCoord.y));
				oldCoord = newCoord;
			}
		}
	}

	IEnumerator UpdateInterfaceElements() {
		while(true) {

			zoomText.text = (myCamera.fieldOfView).ToString();
			posXText.text = (myCamera.transform.localEulerAngles.y).ToString();
			posYText.text = (myCamera.transform.localEulerAngles.x).ToString();
			teamPowerText.text = myCommander.myTeam.teamPower.ToString();
			yield return new WaitForSeconds(1f / interfaceFramerate);
		}
	}

	public void PowerCommanderDrone(bool power) {
		this.power = power ? PowerState.On : PowerState.Off;
	}

	public void ToggleLaserTarget() {
		laserTargetLR.enabled = !laserTargetLR.enabled;
	}
}