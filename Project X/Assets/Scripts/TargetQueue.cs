﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// An enum that represents the way in which a List of AIGoals are sorted.
/// </summary>
public enum SortPattern {AscendingImportance, DescendingImportance, Random, DistanceAscending, DistanceDescending};

public class TargetQueue {
	//TODO: Currently this will complete goals from First to Last.

	public List<AIGoal> goals = new List<AIGoal>();

	public void SortGoals(SortPattern pattern) {
		if(pattern == SortPattern.AscendingImportance) {

		}
	}

}
