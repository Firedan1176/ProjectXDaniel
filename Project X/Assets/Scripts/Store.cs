﻿using System;
using UnityEngine;
using System.Collections.Generic;


/*
	TODO:
	We may only need a list of InventoryItems that are storables, since immediates are immediate.
	Therefore we don't need a list.


*/
[Serializable]
public class Store {

	public List<InventoryItem> storables;
	public List<InventoryItem> immediates;
}
