﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


/*AI, FPS will NOT derive from player, but a subclass called "Soldier?"

TODO:
1.)	Commander shouldn't have to worry about the PlayerSpeed and other FPS info.
	We can move those to a separate class so the structure would look like so:
	Player
		Soldier
			FPS
			AI
		Commander
	
	instead of:
	Player
		FPS
		AI
		Commander
	
2.)	


*/

[Serializable]
public class Player : MonoBehaviour{

	public string playerName;
	public bool isAlive = true;
	public Team myTeam;

	void Start() {
		/*
		STOP!!! All of this is practically useless in Start and Update!
		They're hidden because of inheritance. Fuck.
		We need to rethink what happens in these scripts.

		*/
		if(playerName == string.Empty)
			Debug.LogError("Player '" + gameObject.name + "' doesn't have a name!");
		else name = playerName;

		if(!myTeam)
			Debug.LogError("Player '" + playerName + "' doesn't have a team!");
	}
}