using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

[Serializable]
public class FPS : Soldier {
		

		[SerializeField] private bool isWalking;
		[SerializeField] private float walkSpeed;
		[SerializeField] private float runSpeed;
		[SerializeField] [Range(0f, 1f)] private float runstepLengthen;
		[SerializeField] private float jumpSpeed;
		[SerializeField] private float stickToGroundForce;
		[SerializeField] private float gravityMultiplier;
		[SerializeField] private MouseLook mouseLook;
		[SerializeField] private bool useFovKick;
		[SerializeField] private FOVKick fovKick = new FOVKick();
		[SerializeField] private bool useHeadBob;
		[SerializeField] private CurveControlledBob headBob = new CurveControlledBob();
		[SerializeField] private LerpControlledBob jumpBob = new LerpControlledBob();
		[SerializeField] private float stepInterval;
		[SerializeField] private AudioClip[] footstepSounds;	// an array of footstep sounds that will be randomly selected from.
		[SerializeField] private AudioClip jumpSound;		   // the sound played when character leaves the ground.
		[SerializeField] private AudioClip landSound;		   // the sound played when character touches back on ground.

		private bool jump;
		private float yRotation;
		private Vector2 controlInput;
		private Vector3 moveDir = Vector3.zero;
		private CharacterController myCharacterController;
		private CollisionFlags collisionFlags;
		private bool previouslyGrounded;
		private Vector3 cameraPositionOrigin;
		private float stepCycle;
		private float nextStep;
		private bool jumping;
		private AudioSource myAudioSource;


		//TODO: Organize these;
		Vector3 startHeight;
		public Vector3 crouchDistance;
		public bool crouch;

	// Use this for initialization
	void Start() {
		myCharacterController = GetComponent<CharacterController>();
		myCamera = GetComponentInChildren<Camera>();
		cameraPositionOrigin = myCamera.transform.localPosition;
		fovKick.Setup(myCamera);
		headBob.Setup(myCamera, stepInterval);
		stepCycle = 0f;
		nextStep = stepCycle/2f;
		jumping = false;
		myAudioSource = GetComponent<AudioSource>();
		mouseLook.Init(transform , myCamera.transform);

		startHeight = myCamera.transform.localPosition;

		if(handRoot) handAnimator = handRoot.GetComponent<Animator>();
		if(gunRoot) guns = gunRoot.GetComponentsInChildren<Gun>();

		foreach (Gun g in guns) {
			g.gameObject.SetActive(false);
		}
		SwitchGun(0);

	}


	// Update is called once per frame
	void Update() {
		RotateView();
		// the jump state needs to read here to make sure it is not missed
		if (!jump) {
			jump = CrossPlatformInputManager.GetButtonDown("Jump");
		}
		if (!previouslyGrounded && myCharacterController.isGrounded) {
			StartCoroutine(jumpBob.DoBobCycle());
			PlayLandingSound();
			moveDir.y = 0f;
			jumping = false;
		}
		if (!myCharacterController.isGrounded && !jumping && previouslyGrounded) {
			moveDir.y = 0f;
		}

		previouslyGrounded = myCharacterController.isGrounded;


		crouch = Input.GetAxisRaw("Crouch") == 1;
		

		if(crouch) myCamera.transform.localPosition = crouchDistance;
		else myCamera.transform.localPosition = startHeight;

		int scrollWheelValue_Temp = (int)(Input.GetAxis("Mouse ScrollWheel") * 10);
		if (scrollWheelValue_Temp != 0) {
			SwitchGun(scrollWheelValue_Temp);
		}

		guns[activeGun].isShooting = Input.GetAxisRaw("Fire1") == 1;
	}


		private void PlayLandingSound() {
			myAudioSource.clip = landSound;
			myAudioSource.Play();
			nextStep = stepCycle + .5f;
		}


		private void FixedUpdate() {
			float speed;
			GetInput(out speed);
			// always move along the camera forward as it is the direction that it being aimed at
			Vector3 desiredMove = transform.forward*controlInput.y + transform.right*controlInput.x;

			// get a normal for the surface that is being touched to move along it
			RaycastHit hitInfo;
			Physics.SphereCast(transform.position, myCharacterController.radius, Vector3.down, out hitInfo,
							   myCharacterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
			desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

			moveDir.x = desiredMove.x*speed;
			moveDir.z = desiredMove.z*speed;


			if (myCharacterController.isGrounded)
			{
				moveDir.y = -stickToGroundForce;

				if (jump)
				{
					moveDir.y = jumpSpeed;
					PlayJumpSound();
					jump = false;
					jumping = true;
				}
			}
			else
			{
				moveDir += Physics.gravity*gravityMultiplier*Time.fixedDeltaTime;
			}
			collisionFlags = myCharacterController.Move(moveDir*Time.fixedDeltaTime);

			ProgressStepCycle(speed);
			UpdateCameraPosition(speed);

			mouseLook.UpdateCursorLock();
		}


		private void PlayJumpSound()
		{
			myAudioSource.clip = jumpSound;
			myAudioSource.Play();
		}


		private void ProgressStepCycle(float speed)
		{
			if (myCharacterController.velocity.sqrMagnitude > 0 && (controlInput.x != 0 || controlInput.y != 0))
			{
				stepCycle += (myCharacterController.velocity.magnitude + (speed*(isWalking ? 1f : runstepLengthen)))*
							 Time.fixedDeltaTime;
			}

			if (!(stepCycle > nextStep))
			{
				return;
			}

			nextStep = stepCycle + stepInterval;

			PlayFootStepAudio();
		}


		private void PlayFootStepAudio()
		{
			if (!myCharacterController.isGrounded)
			{
				return;
			}
			// pick & play a random footstep sound from the array,
			// excluding sound at index 0
			int n = Random.Range(1, footstepSounds.Length);
			myAudioSource.clip = footstepSounds[n];
			myAudioSource.PlayOneShot(myAudioSource.clip);
			// move picked sound to index 0 so it's not picked next time
			footstepSounds[n] = footstepSounds[0];
			footstepSounds[0] = myAudioSource.clip;
		}


		private void UpdateCameraPosition(float speed)
		{
			Vector3 newCameraPosition;

			//TODO: Currently, this is leaving if we're crouching (skip headbob), but we should have the headbob be additive.
			//it should to +=, not =. You may need to change HeadBob.DoHeadBob().
			if (!useHeadBob || crouch)
			{
				return;
			}
			if (myCharacterController.velocity.magnitude > 0 && myCharacterController.isGrounded)
			{
				myCamera.transform.localPosition =
					headBob.DoHeadBob(myCharacterController.velocity.magnitude +
									  (speed*(isWalking ? 1f : runstepLengthen)));
				newCameraPosition = myCamera.transform.localPosition;
				newCameraPosition.y = myCamera.transform.localPosition.y - jumpBob.Offset();
			}
			else
			{
				newCameraPosition = myCamera.transform.localPosition;
				newCameraPosition.y = cameraPositionOrigin.y - jumpBob.Offset();
			}
			myCamera.transform.localPosition = newCameraPosition;
		}


		private void GetInput(out float speed)
		{
			// Read input
			float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
			float vertical = CrossPlatformInputManager.GetAxis("Vertical");

			bool waswalking = isWalking;

#if !MOBILE_INPUT
			// On standalone builds, walk/run speed is modified by a key press.
			// keep track of whether or not the character is walking or running
			isWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
			// set the desired speed to be walking or running
			speed = isWalking ? walkSpeed : runSpeed;
			controlInput = new Vector2(horizontal, vertical);

			// normalize input if it exceeds 1 in combined length:
			if (controlInput.sqrMagnitude > 1)
			{
				controlInput.Normalize();
			}

			// handle speed change to give an fov kick
			// only if the player is going to a run, is running and the fovkick is to be used
			if (isWalking != waswalking && useFovKick && myCharacterController.velocity.sqrMagnitude > 0)
			{
				StopAllCoroutines();
				StartCoroutine(!isWalking ? fovKick.FOVKickUp() : fovKick.FOVKickDown());
			}
		}


		private void RotateView()
		{
			mouseLook.LookRotation (transform, myCamera.transform);
		}


		private void OnControllerColliderHit(ControllerColliderHit hit)
		{
			Rigidbody body = hit.collider.attachedRigidbody;
			//dont move the rigidbody if the character is on top of it
			if (collisionFlags == CollisionFlags.Below)
			{
				return;
			}

			if (body == null || body.isKinematic)
			{
				return;
			}
			body.AddForceAtPosition(myCharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
		}
	}