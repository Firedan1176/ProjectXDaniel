﻿using UnityEngine;
using System.Collections;

public enum DamageSource { Soldier, Projectile, Bullet, Console, CheatDetector, Server, Host, Client, None };

public class Soldier : Player {
	/*
	This class should be used for FPS and AI.
	
	Player > Soldier

	*/
	public enum SoldierSpeed { Neutral, Crouch, Walk, Run };
	public enum SoldierState { None, Crouch, Walk, Run };

	public int activeGun = 0;
	public GameObject gunRoot;
	public GameObject handRoot;
	public Gun[] guns;
	public Animator handAnimator;
	public const float maxFireRate = 0.1f;
	public int health;
	public float power;

	public SoldierSpeed currentSpeed = SoldierSpeed.Neutral;
	public SoldierState currentState = SoldierState.None;

	[HideInInspector]
	public Camera myCamera;

	//Detects nearby entities, and Sends a message to them when we've entered range.
	SphereCollider entityDetector;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	/// <summary>
	/// Use this to stop a soldier from responding. This will stop shooting, responses, etc.
	/// </summary>
	public void Stop() {
		guns[activeGun].isShooting = false;
		enabled = false;
	}

	//Getters/Setters
	public string Name {
		get { return name; }
		set { name = value; }
	}

	public int Health {
		get { return health; }
		set { health = value; }
	}
	public float Power {
		get { return power; }
		set { power = value; }
	}


	//public functions go here

	public void TakeDamage(Player source, int amount) {
		if(source.myTeam == myTeam) return;
		Health -= amount;
		if (Health <= 0) {
			PlayerDeathInfo deathInfo = new PlayerDeathInfo(this, source);
			KillPlayer(deathInfo);
		}
	}

	public void KillPlayer(PlayerDeathInfo info) {
		isAlive = false;
		myTeam.RegisterPlayerDeath(info);
		GameObject.Destroy(gameObject);
	}


	/// <summary>
	/// Switch the player's current gun.
	/// </summary>
	/// <param name="offset"></param>
	public void SwitchGun(int offset) {
		//Debug.Log("Switch Gun: " + offset + ", " + activeGun);

		//Special case: Start of game, reset to "default" gun position
		if (offset == 0) {
			activeGun = 0;
			foreach (Gun g in guns) g.gameObject.SetActive(false);
		} else {
			//Normal case: When we scroll wheel.
			guns[activeGun].gameObject.SetActive(false);
			activeGun = ((activeGun + offset) % guns.Length + guns.Length) % guns.Length;
		}
		guns[activeGun].gameObject.SetActive(true);
		guns[activeGun].mySoldier = this;
	}
}
