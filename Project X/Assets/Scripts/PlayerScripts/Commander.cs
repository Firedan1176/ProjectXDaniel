﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Commander : Player {

	public bool bombMap;

	public int currentSelectedItem;
	public DroneController myController;
	
	InventoryItem matchedItem;

	public AudioSource selectItemSound;
	public AudioSource launchMissileSound;

	bool cheatMode = false;
	public float fireRate;
	float fireCountdown;

	// Use this for initialization
	void Start () {
		fireCountdown = fireRate;
		if(myController == null) myController = GetComponent<DroneController>();

		if(bombMap) StartCoroutine(BombMap());
	}

	// Update is called once per frame
	void Update () {
		if(!cheatMode) { if(Input.GetMouseButtonDown(0) && myController.isTargetting && myTeam.CanPurchase(matchedItem.itemCost) && fireCountdown <= 0) UseActiveObject(); }
		else if(cheatMode) { if(Input.GetMouseButton(0) && myController.isTargetting && fireCountdown <= 0) UseActiveObject(); }
		if(Input.GetAxisRaw("Cancel") == 1 && myController.isTargetting) myController.isTargetting = false;

		fireCountdown -= Time.deltaTime;
	}

	public void SelectInventoryItem_Missile(string missileName) {
		//This lets us 'deselect'
		matchedItem = null;
		if (myController.isTargetting) {
			myController.isTargetting = false;
			matchedItem = null;
			return;
		}

		foreach (InventoryItem i in GameManager.Instance.inventoryDatabase.missileGroup.inventoryGroup) {
			if(missileName.Equals(i.itemName)) {
				matchedItem = i;
				break;
			}
		}
		if(matchedItem == null) {
			Debug.LogWarning("Couldn't find '" + missileName + "' missile in InventoryDatabase.");
			return;
		}
		myController.isTargetting = true;
		selectItemSound.Play();
	}

	public void SetCheatMode(bool setting) {
		cheatMode = setting;
	}

	void UseActiveObject() {
		if(matchedItem != null) {
			if(!cheatMode) myTeam.UsePower(matchedItem.itemCost);

			GameObject temp = (GameObject)Instantiate(matchedItem.referenceGameObject, myController.spaceLaunchObject.transform.position, Quaternion.identity);
			if(temp.GetComponent<Missile>()) {
				temp.GetComponent<Missile>().target = myController.targetIcon.transform.position;
				temp.GetComponent<Missile>().myCommander = this;
				launchMissileSound.Play();
			}

		}
		else Debug.LogWarning("Warning: Tried to UseActiveObject, but 'matchedItem' InventoryItem object is null.");
		fireCountdown = fireRate;
	}

	IEnumerator BombMap() {
		matchedItem = GameManager.Instance.inventoryDatabase.missileGroup.inventoryGroup[0];

		for (int x = 0; x < 10; x++) {
			for(int y = 0; y < 10; y++) {
				GameObject tempMissile = (GameObject)Instantiate(matchedItem.referenceGameObject, myController.spaceLaunchObject.transform.position, Quaternion.identity);
				tempMissile.GetComponent<Missile>().target = new Vector3(x, 1, y) * 50;
				launchMissileSound.Play();
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
}