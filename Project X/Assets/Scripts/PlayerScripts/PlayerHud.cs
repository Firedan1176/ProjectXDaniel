﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Holds UI Stats for a Player.
/// </summary>
class PlayerHud {

	public UnityEngine.UI.Text powerText;
	public UnityEngine.UI.Text actionPopupMessage;

}