﻿using UnityEngine;
using System.Collections;


public enum TargetType {Default, Main};

[RequireComponent(typeof(NavMeshAgent))]
public class AI : Soldier {

	public Transform target;
	NavMeshAgent myAgent;
	float navMeshRefreshRate = 0.2f;
	bool canSearch;

	Vector3 shootPosition;

	// Use this for initialization
	void Start () {
		canSearch = true; //Temp. May be changed below. idk.
		if(target == null) {
			Debug.LogError("AI doesn't have an assigned target! Disabling.");
			canSearch = false;
		}
		myAgent = GetComponent<NavMeshAgent>();
		if(canSearch) StartCoroutine(TargetLoop());

	}

	// Update is called once per frame
	void Update () {
		if(!canSearch) return;

		if(myAgent.remainingDistance - myAgent.stoppingDistance <= 0.1f && myAgent.hasPath) {
			guns[activeGun].transform.LookAt(target);
			guns[activeGun].isShooting = true;
		}
		else guns[activeGun].isShooting = false;
	}

	IEnumerator TargetLoop() {
		while(isAlive) {
			if(target == null) target = FindNewTarget().transform;

			//Potentially find a closer enemy
			//TODO: This is performance heavy, the enemy needs access to the players on the other team somehow a more efficient way.
			GameObject closestEnemy = null;
			foreach (Player p in FindObjectsOfType<Player>()) {
				if(Vector3.Distance(transform.position, target.position) > Vector3.Distance(transform.position, p.transform.position) && p.GetComponent<Player>() && !PlayerOnTeam(p.GetComponent<Player>()))
					closestEnemy = p.gameObject;
			}
			if(closestEnemy) target = closestEnemy.transform;

			myAgent.SetDestination(target.position);

			yield return new WaitForSeconds(navMeshRefreshRate);
		}

	}

	GameObject FindNewTarget() {
		GameObject closestObject = null;
		if(GameManager.Instance.GetEnemyTeam(this).aiPlayers.Count == 0) return GameManager.Instance.GetEnemyTeam(this).gameObject;
		foreach (Player p in GameManager.Instance.GetEnemyTeam(myTeam).aiPlayers) {
			if(p.myTeam == myTeam) Debug.LogWarning("FUCK."); //If you got here, this means that we're trying to target a player that's on our team! WTF?
			if((closestObject == null || Vector3.Distance(transform.position, closestObject.transform.position) > Vector3.Distance(transform.position, p.transform.position)) && p.transform != transform) {
				closestObject = p.gameObject;
				//Debug.Log("Reassigned: " + closestObject.name);
			}
			//else Debug.Log("Not a player: " + c.name);
		}
		//Debug.Log(closestObject.name);
		return closestObject;
	}


	/// <summary>
	/// Returns true if the Player passed is on the same team.
	/// </summary>
	/// <param name="player">The Player to be checked.</param>
	/// <returns></returns>
	bool PlayerOnTeam(Player player) {
		return player.myTeam == myTeam;
	}
}


public struct TargetThreatLevel {
	public float threatLevel;
	public TargetType targetType;

	public TargetThreatLevel(float level, TargetType type) {
		this.threatLevel = level;
		this.targetType = type;
	}

}