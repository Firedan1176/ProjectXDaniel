﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class Team : MonoBehaviour {
	

	public Vector3 baseSize;

	public string teamName;
	public Color teamColor;
	public Sprite teamLogo;

	public int score;
	public int teamHealth;
	public int teamPower;

	public Player[] players = new Player[5];
	public int commander;
	public List<Player> aiPlayers = new List<Player>();

	public float aiSpawnDelay = 0.5f;
	public GameObject aiPrefab;
	public Vector3 aiSpawnPoint;

	BoxCollider teamBase;

	public bool spawnEnemiesDebug;
	public int spawnEnemiesDebugCount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(spawnEnemiesDebug) {
			SpawnAI(spawnEnemiesDebugCount);
			spawnEnemiesDebug = false;
		}
	}

	void OnDrawGizmos() {

		Gizmos.color = teamColor;

		//Rotate the Gizmo's matrix.
		Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
		Gizmos.DrawWireCube(baseSize / 2, baseSize + new Vector3(0.1f, 0, 0.1f));
		Gizmos.matrix = Matrix4x4.identity;

		if(teamBase == null) teamBase = GetComponent<BoxCollider>();
		teamBase.center = baseSize / 2;
		teamBase.size = baseSize;

		Gizmos.color = new Color(150, 100, 0);
		Gizmos.DrawWireSphere(transform.position + aiSpawnPoint, 0.3f);
		if(!teamName.Equals(string.Empty) && !GameObject.Find(teamName)) gameObject.name = teamName;
		foreach(Player p in players) if(p != null) p.myTeam = this;
	}

	public void AddScore(int amount) {
		score = Mathf.Clamp(score += amount, 0, GameManager.Instance.scoreLimit);
	}

	public void SpawnAI(int count) {
		StartCoroutine(SpawnAILoop(count));
	}

	public void RegisterPlayerDeath(PlayerDeathInfo info) {
		Debug.Log("Enemy: " + info.thisPlayer.name + " died.");
		aiPlayers.Remove(info.thisPlayer);
		GameManager.Instance.AddTeamPoints(info.thisPlayer.myTeam, 100);
	}

	IEnumerator SpawnAILoop(int count) {
		while(count > 0) {
			GameObject temp = (GameObject)Instantiate (aiPrefab, transform.position + aiSpawnPoint, Quaternion.identity);
			aiPlayers.Add (temp.GetComponent<Player> ());
			Team[] teams = GameObject.FindObjectsOfType<Team> ();
			temp.GetComponent<Player> ().myTeam = this;
			temp.GetComponent<AI> ().target = GameManager.Instance.GetEnemyTeam(this).transform;
			temp.GetComponent<MeshRenderer>().material.color = teamColor;
			temp.GetComponent<Player>().playerName = "AI " + Random.Range(0, 100);
			count--;
			yield return new WaitForSeconds(aiSpawnDelay);
		}
	}

	public void TakeDamage(Player sourcePlayer, int amount) {
		teamHealth -= amount;

		if(teamHealth <= 0) GameManager.Instance.SendMessage("OnTeamDestroyed", this);
	}

	public bool CanPurchase(int power) {
		return teamPower - power >= 0;
	}

	public void UsePower(int power) {
		teamPower -= power;
		if(power <= 0) {
			GameManager.Instance.OnTeamDestroyed(this);
		}
	}
}
