﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum InventoryDatabaseCategory {Missile, Supply};

public class DatabaseButtonFormatter : MonoBehaviour {

	public Commander myCommander;
	public GameObject buttonReference;

	public InventoryDatabaseCategory category;

	// Use this for initialization
	void Awake() {
		if(category == InventoryDatabaseCategory.Missile)
			foreach(InventoryItem i in GameManager.Instance.inventoryDatabase.missileGroup.inventoryGroup) {
				GameObject button = (GameObject)Instantiate(buttonReference);
				button.GetComponentInChildren<Text>().text = i.itemName.ToUpper();
				button.GetComponent<Button>().onClick.AddListener(() => myCommander.SelectInventoryItem_Missile(i.itemName));
				button.transform.SetParent(transform);
			}
		else if(category == InventoryDatabaseCategory.Supply)
			foreach(InventoryItem i in GameManager.Instance.inventoryDatabase.supplyGroup.inventoryGroup) {
				GameObject button = (GameObject)Instantiate(buttonReference);
				button.GetComponentInChildren<Text>().text = i.itemName.ToUpper();
				button.transform.SetParent(transform);
			}
	}
}
