﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Stores information about a player's death such as the victim, the attacker,
/// the number of points to be rewarded, etc.
/// This is used by the GameManager/Team to authenticate death information, save stats, etc.
/// </summary>
public class PlayerDeathInfo {

	public Player thisPlayer;
	public Player attackerPlayer;

	public PlayerDeathInfo(Player thisPlayer, Player attackerPlayer) {
		this.thisPlayer = thisPlayer;
		this.attackerPlayer = attackerPlayer;
	}
}