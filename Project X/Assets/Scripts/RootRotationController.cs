﻿using UnityEngine;
using System.Collections;

public class RootRotationController : MonoBehaviour {

	Soldier mySoldier;

	public Vector2 minBlowback;
	public Vector2 maxBlowback;
	[Range(0.01f, 4f)]
	public float multiplier;
	public float recoverySpeed = 1;
	public bool activeRecovery = false;


	void Start() {
		mySoldier = GetComponentInParent<Soldier>();
	}

	// Update is called once per frame
	void Update () {
		if((!activeRecovery && mySoldier.guns[mySoldier.activeGun].isShooting == false) || activeRecovery) {
			transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, Time.deltaTime * recoverySpeed);
		}
	}

	public void AddBlowback(float amount) {
		amount = Mathf.Clamp(amount, 0, 1);
		transform.localRotation = Quaternion.Euler(transform.localEulerAngles + new Vector3(Random.Range(minBlowback.x, maxBlowback.x), Random.Range(minBlowback.y, maxBlowback.y), 0) * amount * multiplier);
	}
}
