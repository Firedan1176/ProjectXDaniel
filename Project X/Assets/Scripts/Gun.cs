﻿using UnityEngine;
using System.Collections;

public enum BulletType {Raycast, Collision};

public class Gun : Weapon {

	static float maxDistance = 1000;

	public Soldier mySoldier;

	//TODO: Consider Object Pooling.
	public GameObject bulletPrefab;
	Animator myAnimator;
	RootRotationController crc;
	public Transform barrel;
	public float fireRate;
	public float fireRateCountdown;
	public float zoomScale;
	public float accuracy;

	public int powerConsumption;
	public int gunDamage;
	public float gunBlowbackAmount;

	public bool isShooting = false;

	RaycastHit hitInfo;
	public BulletType bulletType;
	AudioSource shootSource;
	public SoundGroup shootSounds;
	public LayerMask collisionLayers;


	//Bullet Decal
	public bool useBulletHoleDecal = false;
	public Sprite bulletHoleDecal;
	public Vector2 bulletHoleDecalSize;
	[Range(0.1f, 1000f)] public float bulletHoleDecalLifetime;

	/// <summary>
	/// Shoots one shot.
	/// </summary>
	/// <param name="power"></param>
	/// <returns></returns>
	void Shoot() {
		Debug.Log(gameObject.name + " just shot");

		if(shootSource)				shootSource.PlayOneShot(shootSounds.GetRandom());
		if(myAnimator)				myAnimator.SetTrigger("Fire");
		if(mySoldier.handAnimator)	mySoldier.handAnimator.SetTrigger("Fire");
		if(crc)						crc.AddBlowback(gunBlowbackAmount);


		Debug.DrawLine(barrel.transform.position, barrel.transform.position + barrel.transform.forward, Color.yellow);

		if(bulletType == BulletType.Raycast) {
			if (Physics.Raycast(barrel.transform.position, barrel.transform.forward, out hitInfo)) {
				if(useBulletHoleDecal) CreateBulletDecal(hitInfo);
				//Debug.Log("Hit! " + hitInfo.transform.name);
				if (hitInfo.transform.GetComponent<Soldier>()) hitInfo.transform.GetComponent<Soldier>().TakeDamage(mySoldier, gunDamage);
				else if (hitInfo.transform.GetComponent<Team>())
					hitInfo.transform.GetComponent<Team>().TakeDamage(mySoldier, gunDamage);
			}
		}

		GameObject temp;
		if (bulletPrefab) {
			temp = (GameObject)Instantiate(bulletPrefab, barrel.transform.position, Quaternion.Euler((Random.onUnitSphere * accuracy) + barrel.transform.eulerAngles));
			temp.transform.SetParent(GameManager.Instance.transform);
			if(bulletType == BulletType.Collision) {
				temp.GetComponent<EffectSettings>().MoveVector = barrel.transform.forward;
				temp.GetComponent<EffectSettings>().sourcePlayer = mySoldier;
				temp.GetComponent<EffectSettings>().damage = gunDamage;
			}
		} else Debug.LogWarning("Bullet Prefab hasn't been asigned to this Player's active gun.");

		fireRateCountdown = fireRate;
	}

	void Start() {
		mySoldier = GetComponentInParent<Soldier>();
		shootSource = GetComponent<AudioSource> ();
		myAnimator = GetComponent<Animator> ();
		fireRateCountdown = fireRate;
		crc = transform.root.GetComponentInChildren<RootRotationController>();
	}

	void Update() {
		if(isShooting) {
			if(fireRateCountdown <= 0)
				if(mySoldier.power - powerConsumption >= 0) Shoot();
				else Debug.LogWarning("Not enough power to shoot!");
		}

		if(fireRateCountdown > 0) fireRateCountdown -= Time.deltaTime;

	}

	void CreateBulletDecal(RaycastHit hitInfo) {
		GameObject temp							= new GameObject("BulletDecal");
		SpriteRenderer sr						= temp.AddComponent<SpriteRenderer>();
		sr.sprite								= bulletHoleDecal;
		temp.transform.localScale				= new Vector3(bulletHoleDecalSize.x, bulletHoleDecalSize.y, 1);
		temp.transform.position					= hitInfo.point + (hitInfo.normal * 0.1f);
		temp.transform.forward					= hitInfo.normal;
		Destroy(temp, bulletHoleDecalLifetime);

		temp.transform.SetParent(GameManager.Instance.transform);

	}
}
