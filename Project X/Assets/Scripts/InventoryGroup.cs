﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class InventoryGroup {

	[SerializeField]
	public List<InventoryItem> inventoryGroup = new List<InventoryItem>();
}
