﻿using UnityEngine;
using System.Collections;

public class PrefabSpawn : MonoBehaviour {

	public GameObject prefab;
	public bool useChildren;
	public float spawnRate;
	public bool useSpawnCount;
	public float spawnCount;
	public float countdown;
	public bool spawnOnStart = false;
	bool isSpawning = false;

	//used via children
	Transform[] spawnpoints;

	// Use this for initialization
	void Start () {
		spawnpoints = GetComponentsInChildren<Transform>();
		if(spawnOnStart) SetSpawning(true);
		Debug.Log("STAT");
	}
	
	// Update is called once per frame
	void Update () {
		if(isSpawning && countdown <= 0) {
			SpawnPrefab();
			countdown = spawnRate;
			if (useSpawnCount) spawnCount--;

		}
		if (isSpawning) countdown -= Time.deltaTime;

	}

	public void SetSpawning(bool value) {
		isSpawning = value;
		countdown = spawnRate;
	}

	private void SpawnPrefab() {
			GameObject child = spawnpoints[Random.Range(0, spawnpoints.Length)].gameObject;
			GameObject temp = (GameObject)Instantiate(prefab, (useChildren?child.transform.position:transform.position), (useChildren?child.transform.rotation:transform.rotation));

		temp.GetComponent<AI>().target = GameObject.Find("Player").transform;
		temp.GetComponent<AI>().myTeam = GameObject.Find("Team2").GetComponent<Team>();
	}
}
