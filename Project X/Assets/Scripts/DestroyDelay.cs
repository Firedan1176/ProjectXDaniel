﻿using UnityEngine;
using System.Collections;

public class DestroyDelay : MonoBehaviour {

	public bool callOnStart;
	public bool poolInstead;
	public float lifetime;

	GameObject poolParent;

	void Start () {
		poolParent = GameManager.Instance.explosionPool;
		if (callOnStart) Trigger();

	}

	public void Trigger() {
		if(poolInstead) {
			StartCoroutine(WaitToPool());
			return;
		}
		Destroy(gameObject, lifetime);
	}

	IEnumerator WaitToPool() {
		yield return new WaitForSeconds(lifetime);
		transform.SetParent(poolParent.transform);
		gameObject.SetActive(false);
	}
}
