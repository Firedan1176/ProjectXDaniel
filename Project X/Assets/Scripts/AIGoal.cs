﻿using UnityEngine;
using System.Collections;

//None: The targt will not move or interact at all.
//Minimal: The target will use the goal as a "last resort".
//Average: The target will choose the goal over a minimal goal.
//Severe: The target will go to the goal, but can interact with non-serious scenarios (i.e. Kill an enemy that is almost dead).
//Urgent: The target will go directly to the goal without any interacting in the environment.
public enum TargetSeverity { None, Minimal, Average, Severe, Urgent };

public class AIGoal {

	public Transform goal;
	public TargetSeverity severity = TargetSeverity.None;


}
