﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour {

	[HideInInspector] public Commander myCommander;

	public Vector3 target;
	public float speed;
	private float oldDist;

	public GameObject collisionObject;

	GameObject explosionObject;
	
	public float explosionRadius = 10;
	public float explosionDamage = 60;
	public bool dealDamage = true;

	// Use this for initialization
	void Start () {
		transform.LookAt(target);
		oldDist = Vector3.Distance(transform.position, target);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * speed * Time.deltaTime;
		if(oldDist < Vector3.Distance(transform.position, target)) Detonate();
		oldDist = Vector3.Distance(transform.position, target);

	}

	void FixedUpdate() {
		if(1.0f / Time.deltaTime > 30f && explosionObject == null && GameManager.Instance.explosionPool.transform.childCount < 5) {
			explosionObject = (GameObject)Instantiate(collisionObject, target, Quaternion.Euler(-transform.forward));
			explosionObject.SetActive(false);
			explosionObject.transform.SetParent(GameManager.Instance.explosionPool.transform);
		}
	}

	void Detonate() {
		foreach(Soldier s in FindObjectsOfType<Soldier>()) {
			float dist = Vector3.Distance(s.transform.position, transform.position);
			if (dist > explosionRadius) {
				continue;
			}
			int effect = (int)(((explosionRadius - dist) / explosionRadius) * explosionDamage);
			Debug.Log("MISSILE: Giving " + effect + " damage!");
			s.TakeDamage(myCommander, effect);
		}

		if (GameManager.Instance.explosionPool.transform.childCount > 0) explosionObject = GameManager.Instance.explosionPool.transform.GetChild(0).gameObject;
		else explosionObject = (GameObject)Instantiate(collisionObject, target, Quaternion.LookRotation(-transform.forward));
		explosionObject.transform.SetParent(null);
		explosionObject.transform.position = transform.position;
		explosionObject.transform.rotation = Quaternion.LookRotation(-transform.forward);
		explosionObject.SetActive(true);
		Destroy(gameObject);
	}
}
