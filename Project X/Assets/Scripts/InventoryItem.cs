﻿using UnityEngine;
using System.Collections;

public enum InventoryItemType { Storable, Immediate };

[System.Serializable]
public class InventoryItem {

	public InventoryItemType type;
	public GameObject referenceGameObject;
	public string itemName;
	public int itemCost;
	
}
