﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum RelativePosition {none, left, right, top, bottom};

public class ProceduralUIAnimations : MonoBehaviour {

	RectTransform myRect;

	Vector3 tempPos;

	public float speed = 2;

	// Use this for initialization
	void Start () {
		myRect = GetComponent<RectTransform>();
	}


	public void TransitionIn(string relativePosition) {
		RelativePosition rp = RelativePosition.none;
		tempPos = myRect.anchoredPosition;
		if (relativePosition.Equals("left")) {
			myRect.pivot = new Vector2(1, 0.5f);
			rp = RelativePosition.left;
		}
		if (relativePosition.Equals("bottom")) {
			myRect.pivot = new Vector2(0.5f, 1);
			rp = RelativePosition.bottom;
		}
		if (relativePosition.Equals("top")) {
			myRect.pivot = new Vector2(0.5f, 0);
			rp = RelativePosition.top;
		}
		if (relativePosition.Equals("right")) {
			myRect.pivot = new Vector2(0, 0.5f);
			rp = RelativePosition.right;
		}
		if (rp == RelativePosition.none) {
			Debug.LogWarning("Cannot transition to none.");
			return;
		}
		StartCoroutine(TransitionInPivot(myRect.pivot, rp));
	}

	public void TransitionOut(string relativePosition) {
		RelativePosition rp = RelativePosition.none;
		tempPos = myRect.anchoredPosition;
		if (relativePosition.Equals("left")) {
			myRect.pivot = new Vector2(0, 0.5f);
			rp = RelativePosition.left;
		}
		if (relativePosition.Equals("bottom")) {
			myRect.pivot = new Vector2(0.5f, 0);
			rp = RelativePosition.bottom;
		}
		if (relativePosition.Equals("top")) {
			myRect.pivot = new Vector2(0.5f, 1);
			rp = RelativePosition.top;
		}
		if (relativePosition.Equals("right")) {
			myRect.pivot = new Vector2(1, 0.5f);
			rp = RelativePosition.right;
		}
		if (rp == RelativePosition.none) {
			Debug.LogWarning("Cannot transition to none.");
			return;
		}
		StartCoroutine(TransitionOutPivot(myRect.pivot, rp));
	}

	IEnumerator TransitionInPivot(Vector2 start, RelativePosition rp) {
		int breakToken = 10000;

		Vector2 end = Vector2.zero;
		if(rp == RelativePosition.left)		end = new Vector2(0, 0.5f);
		if(rp == RelativePosition.right)	end = new Vector2(1, 0.5f);
		if (rp == RelativePosition.top)		end = new Vector2(0.5f, 0);
		if (rp == RelativePosition.bottom)	end = new Vector2(0.5f, 1);

		while (myRect.pivot.normalized != end.normalized) {
			myRect.pivot += (end - start) * speed * Time.deltaTime;
			if(Vector2.Distance(myRect.pivot, end) <= 0.01f + Time.deltaTime * speed) myRect.pivot = end;
			myRect.anchoredPosition = tempPos;
			breakToken--;
			if(breakToken <= 0) {
				Debug.LogWarning("Warning: Waited too long inside a while loop.");
				break;
			}
			yield return null;
		}

	}

	IEnumerator TransitionOutPivot(Vector2 start, RelativePosition rp) {
		int breakToken = 10000;

		Vector2 end = Vector2.zero;
		if (rp == RelativePosition.left)	end = new Vector2(1, 0.5f);
		if (rp == RelativePosition.right)	end = new Vector2(0, 0.5f);
		if (rp == RelativePosition.top)		end = new Vector2(0.5f, 1);
		if (rp == RelativePosition.bottom)	end = new Vector2(0.5f, 0);

		while (myRect.pivot.normalized != end.normalized) {
			myRect.pivot += (end - start) * speed * Time.deltaTime;
			if (Vector2.Distance(myRect.pivot, end) <= 0.01f + Time.deltaTime * speed) myRect.pivot = end;
			myRect.anchoredPosition = tempPos;
			breakToken--;
			if (breakToken <= 0) {
				Debug.LogWarning("Warning: Waited too long inside a while loop.");
				break;
			}
			yield return null;
		}
		Debug.Log("Finished");

	}
}
