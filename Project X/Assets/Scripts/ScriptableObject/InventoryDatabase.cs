﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "InventoryDatabase", menuName = "Database/Create New Inventory Database...", order = 1)]
public class InventoryDatabase : ScriptableObject {

	public InventoryGroup missileGroup = new InventoryGroup();
	public InventoryGroup supplyGroup = new InventoryGroup();

}
