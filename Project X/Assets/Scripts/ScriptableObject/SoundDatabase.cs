﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "SoundDatabase", menuName = "Database/Create New Sound Database...")]
public class SoundDatabase : ScriptableObject {
	public List<AudioClip> clips = new List<AudioClip>();

}
