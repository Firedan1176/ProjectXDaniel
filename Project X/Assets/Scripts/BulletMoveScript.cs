﻿using UnityEngine;
using System.Collections;

public class BulletMoveScript : MonoBehaviour {
	public int lifetime;
	public float speed = 20;
	public GameObject collisionEffect;

	public Vector3 endLocation; //create collisionEffect when we reach this (or close to it)


	void Start() {
		GameObject.Destroy(gameObject, lifetime);
	}

	void Update() {
		transform.position += transform.forward * speed * Time.deltaTime;
		if (Vector3.Distance(transform.position, endLocation) <= speed * Time.deltaTime * 2)
			CreateCollisionEffect();
	}

	void CreateCollisionEffect() {
		Instantiate(collisionEffect, endLocation, Quaternion.identity);
	}
}
