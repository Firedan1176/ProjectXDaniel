﻿using UnityEngine;
using System.Collections;

public class ExplosionController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<AudioSource>().clip = GameManager.Instance.explosionSoundDatabase.clips[Random.Range(0, GameManager.Instance.explosionSoundDatabase.clips.Count)];
		GetComponent<AudioSource>().Play();
	}

}
