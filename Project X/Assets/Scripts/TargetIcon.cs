﻿using UnityEngine;
using System.Collections;

public class TargetIcon : MonoBehaviour {

	public Light targetLight;
	public GameObject targetObject;

	public float targetSpinSpeed;
	public Color targetColor;

	// Use this for initialization
	void Start () {
		targetLight.color = Color.Lerp(targetColor, Color.white, 0.3f); //This is to make the light more white. Only used 1 frame
		foreach(SpriteRenderer r in targetObject.GetComponentsInChildren<SpriteRenderer>())
			r.color = targetColor;
	}
	
	// Update is called once per frame
	void Update () {
		targetObject.transform.Rotate(Vector3.forward, targetSpinSpeed);

	}
}
