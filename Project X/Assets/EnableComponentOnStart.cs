﻿using UnityEngine;
using System.Collections;

public class EnableComponentOnStart : MonoBehaviour {

	public MonoBehaviour monoBehaviour;

	// Use this for initialization
	void OnEnable () {
		monoBehaviour.enabled = true;
	}
}
