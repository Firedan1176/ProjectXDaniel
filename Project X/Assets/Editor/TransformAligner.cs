﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class TransformAligner : Editor {


	[MenuItem("GameObject/Transform/Clamp Decimals")]
	static void ClampDecimals() {
		foreach(GameObject go in Selection.gameObjects) {
			Vector3 rounded = go.transform.position;
			go.transform.position = new Vector3((int)rounded.x, (int)rounded.y, (int)rounded.z);
		}
	}

	[MenuItem("GameObject/Transform/Clamp Decimals", true)]
	static bool ValidateClampDecimals() {
		return Selection.gameObjects.Length > 0;
	}

	[MenuItem("GameObject/Transform/Round Decimals")]
	static void RoundDecimals() {
		foreach(GameObject go in Selection.gameObjects) {
			Vector3 rounded = go.transform.position;
			go.transform.position = new Vector3(Mathf.RoundToInt(rounded.x), Mathf.RoundToInt(rounded.y), Mathf.RoundToInt(rounded.z));

		}
	}

	[MenuItem("GameObject/Transform/Round Decimals", true)]
	static bool ValidateRoundDecimals() {
		return Selection.gameObjects.Length > 0;
	}
}
