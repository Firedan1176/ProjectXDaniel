﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(InventoryDatabase))]
public class InventoryDatabaseEditor : Editor {

	//SerializedObject obj;
	//SerializedProperty missileGroup;
	//SerializedProperty supplyGroup;

	//[MenuItem("InventoryItem/Create Inventory Database")]
	//static void CreateInventoryDatabase() {
	//	string path = EditorUtility.SaveFilePanelInProject("Create Inventory Database", "untitled.asset", "asset", "Please enter the name for the new inventory database");

	//	if (string.IsNullOrEmpty(path)) return;

	//	InventoryDatabase db = CreateInstance<InventoryDatabase>();
	//	AssetDatabase.CreateAsset(db, path);
	//	AssetDatabase.SaveAssets();
	//}

	//void OnEnable() {
	//	obj = new SerializedObject(target);
	//	missileGroup = obj.FindProperty("missileGroup");
	//	supplyGroup = obj.FindProperty("supplyGroup");

	//}

	//public override void OnInspectorGUI() {
	//	obj.UpdateIfDirtyOrScript();
	//	EditorGUILayout.PropertyField(missileGroup, new GUIContent("Missile Group", "The inventory database for missile items."), true);
	//	EditorGUILayout.PropertyField(supplyGroup, new GUIContent("Support Group", "The inventory database for supply items."), true);
	//	obj.ApplyModifiedProperties();
	//}
}
