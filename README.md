# Project X

## About
Project X is an indie developed mix between a Real Time Strategy and a First Person Shooter style game.
The project is a combination of a single player and multiplayer environment, with more possibilities in the future.
This repository is dedicated to the Unity3D Project folder, in which the source code, assets, and details
for this game are stored.

## Access
This repository is intended to only be viewed by programmers who need to commit changes to the main project.
Therefore, artists, modelers, story designers, etc. should not have access to edit this repository.
Please note that this repository is meant to contain the original Unity3D Project, and commits should only
be made when necessary.

## Calendar
The information provided below will give dates and notes to events about the project.
| Status   | Date    | Message                              |
|:--------:|:------- | ------------------------------------ |
| Start    | 1/17/16 | First commit to main repository      |
| Start    | 1/25/16 | First commit with AI Testing Project |

## Links
Below are links to various resources.
### Repository
[/GameStuff/](/GameStuff/)
[/Project X AI Testing/](/Project X AI Testing/)
[/Project X AI Testing/Assets/](/Project X AI Testing/Assets/)
[/Project X/](/Project X/)
[/Project X/Assets/](/Project X/Assets/)
### External
#### MarI/O Neuroevolution through Augmenting Topologies (NEAT)
[Research Article](http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf)
[Video Demonstration](https://www.youtube.com/watch?v=qv6UVOQ0F44)
[Source Code](http://pastebin.com/ZZmSNaHX)
#### AI Decision Making
[Artificial Intelligence for Game: Decision Making](https://www.cs.umd.edu/class/spring2013/cmsc425/Lects/lect20.pdf)

# Contacts
## Programmers
Daniel Moore
    __Firedan1176@gmail.com__
Matthew Cliatt

## Artists
Skylar Kelley